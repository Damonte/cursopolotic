"""
Definition of forms.
"""
from django.forms.widgets import *
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from app.models import *

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))
class ProductForm(forms.ModelForm):
    #name = forms.CharField(max_length=100, required=True)
    #description = forms.CharField(widget=forms.Textarea, required=True)
    #price = forms.FloatField(required=True)
    #category = forms.ModelChoiceField(queryset = Category.objects.all(), required=True)
    #image = forms.ImageField()

    class Meta:
        model = Product
        fields = ('name', 'description', 'price', 'category', 'image')
        

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control'})
        #self.fields['description'].widget = forms.TextInput(attrs={'class':'form-control'}),
        self.fields['description'].widget.attrs.update({'class': 'form-control'})
        self.fields['price'].widget.attrs.update({'class': 'form-control'})
        self.fields['category'].widget.attrs.update({'class': 'form-control'})
        self.fields['image'].widget.attrs.update({'class': 'form-control'})
