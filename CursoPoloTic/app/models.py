"""
Definition of models.
"""

from django.db import models
#from django.utils.translation import gettext as _
from django.utils.translation import ugettext_lazy  as _
from django.contrib.auth.models import User

# Create your models here.

#class Category(models.IntegerChoices):
#    FERRETERIA = 1, _('FERRETERIA')
#    COMESTIBLES = 2, _('COMESTIBLES')
#    ELECTRONICOS = 3, _('ELECTRONICOS')
#    PERFUMERIA = 4, _('PERFUMERIA')
#    LIMPIEZA = 5, _('LIMPIEZA')
#    BAZAR = 6, _('BAZAR')

class Category(models.Model):
    description = models.TextField(
            max_length = 50,
        )

    def __str__(self):
        return self.description


class Product(models.Model):
    name = models.CharField(
        max_length=50,
        help_text='Ingrese el nombre del produdcto.'
        )
    
    description = models.CharField(
        max_length=200,
        help_text='Ingrese la descripcion del producto'
        )
    
    price = models.FloatField(
        help_text='Precio del producto'
        )
    
    image = models.ImageField(upload_to="products_images")   #(null=True, blank=True)

    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)

    #category = models.IntegerField(choices=Category.choices)

    def __str__(self):
        return self.name

class Cart(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    products = models.ManyToManyField(Product)

    _total = models.FloatField()

    @property
    def total(self):
        if self.products is None:
            return 0
        else:
            totalAmount = 0.0
            
            for product in self.products:
                totalAmount = totalAmount + product.price
            
            return totalAmount

