"""
Definition of views.
"""

from datetime import datetime
from django.shortcuts import render
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from app.models import *
from django import *
from app.forms import *

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    products = Product.objects.all().order_by('-id')[:6]
    
    return render(
        request,
        'app/index.html',
        {
           'title':'Home Page',
           'year':datetime.now().year,
           'products': products,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contacto',
            'message':'Puede utilizar la siguiente información para contactarnos:',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        product = Product()
        form = ProductForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/about/')
        else:
            return HttpResponseRedirect('/about/')
    else:
        assert isinstance(request, HttpRequest)
        form = ProductForm()
        return render(
            request,
            'app/about.html',
            {
                'title':'Acerca de Jaguarete KAA S.A.',
                'message':'Sobre nosotros:',
                'year':datetime.now().year,
                'form': form,
            }
        )
